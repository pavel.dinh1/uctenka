﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Vu Dinh Hoang
/// </summary>
namespace EvidenceUctenka
{
 public class Program
 {
  static void Main(string[] args)
  {
   // Menu pro tvorbu Uctenka
   Menu m = new Menu();
   do
   {
    m.OpenMenu();
   } while (!m.Quit);
  }
 }
}
