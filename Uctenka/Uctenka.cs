﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

/// <summary>
/// Vu Dinh Hoang
/// </summary>

namespace EvidenceUctenka
{
 public class Uctenka
 {
  private List<Zbozi> zbozi = new List<Zbozi>();
  private string nazevProvozovny;
  private string adresa;
  private int telefon;
  private int ic;
  private string dic;
  private int cisloUctenky;
  private DateTime datum;
  private float celkCastka;

  public float CelkCastka { get { return celkCastka; } set { if (value > 0) celkCastka = value; } }
  public int CisloUctenky { get { return cisloUctenky; } }
  public string NazevProvozovny { get { return nazevProvozovny; } }
  public List<Zbozi> Zbozi { get { return zbozi; } }

  public Uctenka(string nazevProv, string adr, int cislUct, int tel, int ic, string dic, DateTime dat)
  {
   nazevProvozovny = nazevProv;
   adresa = adr;
   telefon = tel;
   this.ic = ic;
   this.dic = dic;
   datum = dat;
   cisloUctenky = cislUct;
  }

  public override string ToString()
  {
   return "Název provozovny: " + nazevProvozovny + "    " + "\n" +
          "Číslo účtenky: " + cisloUctenky.ToString("D7") + "    " + "\n" +
          "Adresa: " + adresa + "    " + "\n" +
          "Telefon: " + telefon + "    " + "\n" +
          "IČ: " + ic + "    " + "\n" +
          "DIČ: " + dic + "    " + "\n" +
          "Datum: " + datum.ToString("dd/MM/yyyy") + "    " + "\n" +
          "Čas: " + datum.ToString("hh:mm") + "    " + "\n\n";
  }

  public float SpocitejCelkCenu()
  {
   foreach (Zbozi z in zbozi)
   {
    CelkCastka += z.Cena;
   }
   return CelkCastka;
  }
 }
}
