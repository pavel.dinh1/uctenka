﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvidenceUctenka
{
 public class Zbozi
 {
  private string jmenoPolozky;
  private int cisloPolozky;
  private float cena;
  private int dph;

  // Properties
  public float Cena
  {
   get { return cena; }
   set
   {
    if (value > 0)
     cena = value;
   } // Ceny jsou pouze kladné
  }
  public int Dph
  {
   get { return dph; }
   set
   {
    switch (value)
    {
     case 21:
     case 15:
     case 10:
      Dph = value;
      break;
     default:
      throw new Exception("Tohle DPH je neplatny!");
    }
   } // DPH - 10,15,21
  }
  public float SpocitejDanZbozi { get { return (float)dph / 100f * cena; } }

  public Zbozi(string jmPol, int cislPol, float cena, int dph)
  {
   this.jmenoPolozky = jmPol;
   this.cisloPolozky = cislPol;
   this.cena = cena;
   this.dph = dph;
  }

  public override string ToString()
  {
   return "Číslo položky: " + cisloPolozky + " Jméno položky: " + jmenoPolozky + " Cena: " + cena;
  }
 }
}

