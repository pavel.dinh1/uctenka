﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace EvidenceUctenka
{
 public class Menu
 {
  int cisloUctenky = 0;
  bool quit = false;
  public bool Quit { get { return quit; } }
  public List<Uctenka> seznamUctenek = new List<Uctenka>();

  public void OpenMenu()
  {
   Console.WriteLine("Generátor účtenek \n\n" +
    "Vytvořit účtenku - 1 \n" +
    "Přidej zboží do účtenky - 2 \n" +
    "Ulož účtenku do textového souboru - 3 \n" +
    "Ukončit program - 0 \n");
   Console.Write("Zadej možnost : ");

   int input = InputConvertInt();
   switch (input)
   {
    case 0:
     Console.WriteLine("Konec");
     quit = true;
     break;
    case 1:
     VytvorUctenku();
     break;
    case 2:
     if (seznamUctenek.Count > 0)
      PridejZbozi();
     else
      Console.WriteLine("Nemas uctenku. Zaloz nejdriv uctenku");
     break;
    case 3:
     Console.WriteLine("Ukladam");
     break;
    default:
     throw new Exception("Špatně jsi zadal/a možnost, zkus to znova !");
   }
  }

  private int InputConvertInt()
  {
   string val = Console.ReadLine();
   return Convert.ToInt32(val);
  }

  private void VytvorUctenku()
  {
   bool close = false;
   do
   {
    Console.Write("Nazev provozovny: ");
    string nazProv = Console.ReadLine();

    Console.Write("Adresa: ");
    string adr = Console.ReadLine();

    Console.Write("Telefon: ");
    int tel = Convert.ToInt32(Console.ReadLine());

    Console.Write("IČ: ");
    int ic = Convert.ToInt32(Console.ReadLine());

    Console.Write("DIČ: ");
    string dic = Console.ReadLine();

    Uctenka u = new Uctenka(nazProv, adr, cisloUctenky, tel, ic, dic, DateTime.Now);
    seznamUctenek.Add(u);
    // Pridavam hodnotu cisla o 1 vysi
    Console.Write("\nChcete přidat další účtenku ?(y/n): ");
    close = CloseLoopDecision(close); // Zeptat se uzivatele zda-li vytvaret dal uctenky
    cisloUctenky++;
   } while (!close);
  }

  private void PridejZbozi()
  {
   bool close = true; // pro uzavreni opakovaneho pridavani zbozi do uctenky
   if (seznamUctenek.Count != 0) // Musi existovat uctenka, aby se tam mohlo vkladat zbozi
   {
    foreach (Uctenka uc in seznamUctenek)
    {
     Console.WriteLine(uc.NazevProvozovny + " " + uc.CisloUctenky);
    }// Vypis uctenek
   }
   Console.Write("Vyber účtenku: ");
   int uct = Convert.ToInt32(Console.ReadLine());

   do
   {
    int cisloPolozky = 0;

    Console.WriteLine("Zpět do menu - 0");
    Console.Write("Zadej zbozi, které chceš zadat do účtenky : ");
    string jmenoPolozky = Console.ReadLine();
    if (jmenoPolozky == "0") { return; }

    Console.WriteLine("Zpět do menu - 0");
    Console.Write("Zadej cenu zboží: ");
    int cena = Convert.ToInt32(Console.ReadLine());
    if (cena == 0) { return; }

    Console.WriteLine("Zpět do menu - 0");
    Console.Write("DPH: ");
    int dph = Convert.ToInt32(Console.ReadLine());
    if (dph == 0) { return; }

    Zbozi z = new Zbozi(jmenoPolozky, cisloPolozky, cena, dph);
    seznamUctenek[uct].Zbozi.Add(z);
    cisloPolozky++;

    Console.Write("\nChcete přidat další zboží ?(y/n): ");
    close = CloseLoopDecision(close);
   } while (!close); // Pridavani parametru ke zbozi
   ZapisUctenkyDoTxt(uct);
  }

  private void ZapisUctenkyDoTxt(int uct)
  {
   string[] zb = new string[seznamUctenek[uct].Zbozi.Count]; // ulozit seznam zbozi do pole textu
   StreamWriter file = new StreamWriter(@"F:\Programming\C#\Uctenka\Uctenky.txt", true);
   file.Write("Účtenka " + uct + "\n" + seznamUctenek[uct].ToString()); // Vypise uctenku
   for (int i = 0; i < zb.Length; i++)
   {
    zb[i] = seznamUctenek[uct].Zbozi[i].ToString(); // Projde kazde zbozi v konkretni uctence a zapise do pole stringu
    file.WriteLine(zb[i]);
   }
   file.WriteLine();
   file.Close();
  }

  private static bool CloseLoopDecision(bool close)
  {
   string option = Console.ReadLine();
   option.ToLower();
   if (option == "y")
    close = false;
   else if (option == "n")
    close = true;
   else
    throw new Exception("Špatný vstup - lze vybrat pouze y/n");
   return close;
  }
 }
}
