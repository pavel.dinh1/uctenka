using System;
using Xunit;
using EvidenceUctenka;

namespace UnitTestUctenka
{
 public class UnitTest1
 {
  [Fact]
  public void TestDanZbozi()
  {
   Zbozi zb = new Zbozi("", 1, 100, 21);
   Assert.Equal(21f, zb.SpocitejDanZbozi);
  }

  [Fact]
  public void TestCelkovaCena()
  {
   Uctenka uc = new Uctenka("","",231,657894651,521,"",DateTime.Now);
   for (int i = 0; i < 10; i++)
   {
    Zbozi zb = new Zbozi("", i, 100, 21);
    uc.Zbozi.Add(zb);
   }
   Assert.Equal(1000, uc.SpocitejCelkCenu());
  }

  [Fact]
  public void TestPocetZboziVUctence()
  {
   Uctenka uc = new Uctenka("", "", 231, 657894651, 521, "", DateTime.Now);
   for (int i = 0; i < 10; i++)
   {
    Zbozi zb = new Zbozi("", i, 100, 21);
    uc.Zbozi.Add(zb);
   }
   Assert.Equal(10, uc.Zbozi.Count);
  }

  [Fact]
  public void TestSatnyDPH()
  {
   Zbozi z = new Zbozi("", 1, 100, 21);
   z.Dph = 0; // Test omezeni DPH -> 10%,15%,21%
   Assert.Equal(0, z.Dph);
  }
 }
}
